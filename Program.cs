﻿using System;

namespace ArrayMaximalAdjacentDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        int arrayMaximalAdjacentDifference(int[] inputArray) {
            int max = 0;
            
            for(int i = 1; i < inputArray.Length - 1; i++){
                    if(Math.Abs(inputArray[i] - inputArray[i - 1]) > max){
                        max = Math.Abs(inputArray[i] - inputArray[i - 1]);
                    }
                    if(Math.Abs(inputArray[i] - inputArray[i + 1]) > max){
                        max = Math.Abs(inputArray[i] - inputArray[i + 1]);
                    }
            }
            return max;
        }

    }
}
